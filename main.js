document.getElementById('addForm').addEventListener('submit',saveOutcome);

function saveOutcome(e) {
  var outcomeAmount = document.getElementById('amountInput').value;
  var outcomeCategory = document.getElementById('categoryInput').value;
  var outcomeWhere = document.getElementById('whereInput').value;
  var outcomeDescription = document.getElementById('descriptionInput').value;
  var outcomeID = chance.guid();

  var outcome = {
    id: outcomeID,
    amount: outcomeAmount,
    category: outcomeCategory,
    where: outcomeWhere,
    description: outcomeDescription
  }

  if(localStorage.getItem('outcomes') == null) {
    var outcomes = [];
    outcomes.push(outcome);
    localStorage.setItem('outcomes', JSON.stringify(outcomes));
  } else {
    var outcomes = JSON.parse(localStorage.getItem('outcomes'));
    outcomes.push(outcome);
    localStorage.setItem('outcomes', JSON.stringify(outcomes));
  }

  document.getElementById('addForm').reset();

  fetchOutcomes();

  e.preventDefault();
}

function deleteOutcome(id) {
  var outcomes = JSON.parse(localStorage.getItem('outcomes'));

  for (var i = 0; i < outcomes.length; i++) {
    if (outcomes[i].id == id) {
      outcomes.splice(i, 1);
    }
  }
  localStorage.setItem('outcomes', JSON.stringify(outcomes));

  fetchOutcomes();
}

function fetchOutcomes() {
  var outcomes = JSON.parse(localStorage.getItem('outcomes'));
  var outcomesList = document.getElementById('outcomesList');

  if(outcomes.length == 0) {
    outcomesList.innerHTML = '<div class="well"><h3>List empty</h3></div>';
  } else {
    outcomesList.innerHTML = '';
  }

  for(var i=0; i<outcomes.length; i++) {
    var id = outcomes[i].id;
    var amount = outcomes[i].amount;
    var category = outcomes[i].category;
    var where = outcomes[i].where;
    var description = outcomes[i].description;
    outcomesList.innerHTML += '<div class="card card-body">'+
                              '<p>ID: '+id+'</p>'+
                              '<p><span class="label label-info">'+amount+'</span></p>'+
                              '<p><span class="label label-info">'+category+'</span></p>'+
                              '<p><span class="label label-info">'+where+'</span></p>'+
                              '<h3>'+description+'</h3>'+
                              '<a href="#" onclick="deleteOutcome(\''+id+'\')" class="btn btn-danger">Delete</a>'+
                              '</div>';
  }
}
